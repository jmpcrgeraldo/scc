#!/bin/bash

# Used to check when a new deployed version is available

STATUS=$(curl -sL -w "%{http_code}" -I "https://scc-backend-12345.azurewebsites.net/clear" -o /dev/null)
echo "GOT $STATUS"
STATUS=$(curl -sL -w "%{http_code}" -I "https://scc-backend-12345-us.azurewebsites.net/clear" -o /dev/null)
echo "GOT $STATUS"

#az storage blob delete-batch --account-name sccstore49543westeu --source image
#az storage blob delete-batch --account-name sccstore49543westus --source image
