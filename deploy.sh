#!/bin/bash

# az login
# az account set --subscription id_returned_in_login


# cp /home/david/snap/azure-cli/7/.azure/azureProfile.json /home/david/.azure/
# cp /home/david/snap/azure-cli/7/.azure/accessTokens.json /home/david/.azure/

# mvn azure-webapp:config

# docker run -it -v $(pwd):/root nunopreguica/ccs-1920


if [[ $# != 3 ]]; then
  echo "usage: eu/us web/func/all log/none"
  exit 0
fi

if [[ "$1" == "eu" ]]; then
  echo "Using EU"
  cp -rf pomEU.xml pom.xml
  printf "LOC=westeu\nFILE=azurekeys-westeu.props" >props/geolocation.props
elif [[ "$1" == "us" ]]; then
  echo "Using US"
  cp -rf pomUS.xml pom.xml
  printf "LOC=westus\nFILE=azurekeys-westus.props" >props/geolocation.props
else
  echo "Wrong arg $1"
fi

mvn package clean

if [[ "$2" == "web" ]]; then
  echo "Deploying web app"
  mvn -Pwebapp package azure-webapp:deploy
elif [[ "$2" == "func" ]]; then
  mvn package azure-functions:deploy
elif [[ "$2" == "all" ]]; then
  mvn -Pwebapp package azure-webapp:deploy
  mvn package azure-functions:deploy
fi

if [[ "$3" == "loc" ]]; then
  echo "Logg Stream"
  az webapp log tail --name scc-backend-12345 --resource-group scc-backend-westeu-12345
fi
