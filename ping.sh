#!/bin/bash

# Used to check when a new deployed version is available

STATUS=100
while [[ $STATUS != 200 ]]; do

  STATUS=$(curl -sL -w "%{http_code}" -I "https://scc-backend-12345-us.azurewebsites.net/v$1/" -o /dev/null)

  echo "GOT $STATUS for $1."

  sleep 5
done

echo "SERVER IS UPDATED"
