#!/bin/bash

az group create --name scc1920-cluster-12345 --location westeurope
az ad sp create-for-rbac --name scc12345-admin

{
  "appId": "8a46f2cc-33fb-4fbb-9227-52a369667656",
  "displayName": "scc12345-admin",
  "name": "http://scc12345-admin",
  "password": "6125b0a7-d935-455e-b189-fd9288f93d72",
  "tenant": "b6682087-da84-4dff-bc54-e3c324fa96ce"
}


az aks create --resource-group scc1920-cluster-12345 --name cluster-12345 \
  --node-vm-size Standard_B2s --node-count 1 --enable-addons monitoring \
  --generate-ssh-keys --service-principal 8a46f2cc-33fb-4fbb-9227-52a369667656 \
  --client-secret a68253ff-78bb-4309-91c5-682b3f069427

az aks get-credentials --resource-group scc1920-cluster-12345 --name cluster-12345

kubectl get nodes

kubectl apply -f backend_k8.yaml

kubectl get service cc-app-k8 --watch

kubectl delete deployments,services,pods --all

http://23.97.247.40:8080/scc-backend-12345/v1
http://23.97.247.40:8080/scc-webapp-12345/v1

