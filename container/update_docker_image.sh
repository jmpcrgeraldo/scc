#!/bin/bash

application="scc-backend-12345"
replica_spec=$(kubectl get deployment/"$application" -o jsonpath='{.spec.replicas}')
kubectl scale --replicas=0 deployment "$application"
kubectl scale --replicas="$replica_spec" deployment "$application"

echo "Done"