'use strict';

/***
 * Exported functions to be used in the testing scripts.
 */
module.exports = {
    genNewUser,
    genNewLikes,
    genNewUserReply,
    genNewCommunity,
    genNewPost,
    genNewPostReply,
    setNewPostImageBody,
    genNewImageReply,
    getImageReply,
    hasMoreInBrowseList,
    hasMoreInImageList,
    selectAllFromPostList,
    selectFromPostList,
    selectFromPostThread,
    startBrowse,
    endBrowse,
    setSearchPar
};


const Faker = require('faker');
const fs = require('fs');
const fetch = require('node-fetch');

let userNames = [];
let userIds = [];
let communityNames = [];
let postIds = [];
let myPostIds = [];
let images = [];

// All endpoints starting with the following prefixes will be aggregated in the same for the statistics
let statsPrefix = [
    ["/pages/thread/", "GET"],
    ["/pages/all/", "GET"],
    ["/post/like/", "POST"],
    ["/post/unlike/", "POST"],
    ["/post/", "GET"],
    ["/users/", "GET"],
    ["/community/", "GET"],
    ["/media/", "GET"],
    ["/search/", "POST"]
];

// Function used to compress statistics
global.myProcessEndpoint = function (str, method) {
    for (let i = 0; i < statsPrefix.length; i++) {
        if (str.startsWith(statsPrefix[i][0]) && method === statsPrefix[i][1])
            return method + ":" + statsPrefix[i][0];
    }
    return method + ":" + str;
};

// Auxiliary function to select an element from an array
Array.prototype.sample = function () {
    return this[Math.floor(Math.random() * this.length)]
};

// Returns a random value, from 0 to val
function random(val) {
    return Math.floor(Math.random() * val)
}

// Loads data about users, communities and images from disk
// This data is written when users and communities are saved
function loadData() {
    if (userNames.length > 0)
        return;
    let str;
    if (fs.existsSync('usernames.data')) {
        str = fs.readFileSync('usernames.data', 'utf8');
        userNames = JSON.parse(str);
    }
    if (fs.existsSync('postids.data')) {
        str = fs.readFileSync('postids.data', 'utf8');
        myPostIds = JSON.parse(str);
    }
    if (fs.existsSync('userids.data')) {
        str = fs.readFileSync('userids.data', 'utf8');
        userIds = JSON.parse(str);
    }
    if (fs.existsSync('communitynames.data')) {
        str = fs.readFileSync('communitynames.data', 'utf8');
        communityNames = JSON.parse(str);
    }
    let i;
    let basefile;
    if (fs.existsSync('/images'))
        basefile = '/images/cats.';
    else
        basefile = 'images/cats.';
    for (i = 1; i <= 40; i++) {
        const name = "cats" + i + '.jpeg';
        let img = fs.readFileSync(basefile + i + '.jpeg');
        const imagesNames = {};

        imagesNames["img"] = img;
        imagesNames["name"] = name;

        images.push(imagesNames)
    }
}

loadData();

/**
 * Generate data for a new user using Faker
 */
function genNewUser(context, events, done) {
    const name = `${Faker.name.firstName()}.${Faker.name.lastName()}`;
    context.vars.name = name;
    userNames.push(name);
    fs.writeFileSync('usernames.data', JSON.stringify(userNames));

    context.vars.email = `${Faker.internet.email()}`;
    context.vars.password = `${Faker.internet.password()}`;

    return done()
}

/**
 Generate likes
 */
function genNewLikes(context, events, done) {
    loadData();

    const postId = myPostIds.sample();
    const userId = userNames.sample();
    context.vars.postId = postId;
    context.vars.userId = userId;
    return done()
}

/**
 * Process reply for of new users to store the id on file
 */
function genNewUserReply(requestParams, response, context, ee, next) {
    if (response.statusCode >= 200 && response.statusCode < 300 && response.body.length > 0) {
        userIds.push(response.body);
        fs.writeFileSync('userids.data', JSON.stringify(userIds));
    }
    return next()
}

/**
 * Generate data for a new community using Faker
 */
function genNewCommunity(context, events, done) {
    const name = `${Faker.lorem.word()}${Faker.lorem.word()}`;
    context.vars.name = name;
    communityNames.push(name);
    fs.writeFileSync('communitynames.data', JSON.stringify(communityNames));

    context.vars.desc = `${Faker.lorem.sentence()}`;
    context.vars.creator = userNames.sample();

    return done()
}

/**
 * Generate data for a new post. Starts by loading data if it was not loaded yet.
 * Stores in the variables:
 * "cammunity" : the name of a community
 * "creator" : the name of a user
 * "msg" : the contents for the message
 * "parentId" : the identifier of a post, so that this post is a reply to that
 * "hasImage" : true/false, depending on whether there will be an image in the post
 * "image" : the contents of the selected image
 */
function genNewPost(context, events, done) {
    loadData();

    context.vars.creationTime = Date.now();
    context.vars.creator = userNames.sample();
    context.vars.community = communityNames.sample();
    context.vars.msg = `${Faker.lorem.paragraph()}`;
    if (postIds.length > 0 && Math.random() < 0.8) {  // 80% are replies
        let npost = postIds.sample();
        context.vars.parentId = npost[0];
        context.vars.community = npost[1];
        context.vars.title = null
    } else {
        context.vars.title = `${Faker.lorem.words()}`;
        context.vars.parentId = null
    }
    context.vars.hasImage = false;
    if (Math.random() < 0.2) {   // 20% of the posts have images
        if (images.length !== 0) {
            const img = images.sample();
            //context.vars.imageId = img["img"];
            context.vars.imageBytes = img["img"];
            context.vars.imageName = img["name"];
            context.vars.hasImage = true
        }
    }

    return done()
}

/**
 * Select next post to read and store information in the following variables:
 * "nextid" - id of the next post to browse
 * "hasNextid" - true/false whether there is any other post to browse next
 * "browsecount" - update information on the session size

 */
function hasMoreInBrowseList(context, next) {
    stillToRead(context);
    return next(context.vars.hasNextid)
}


/**
 * Function that checks if there are more images to load. Axuiliary function.
 * Store information in the following variables:
 * "nextid" - id of the next post to browse
 * "hasNextid" - true/false whether there is any other post to browse next
 */
function checkHasMoreInImageList(context) {
    context.vars.hasNextimageid = false;
    if (context.vars.postlistimages !== undefined)
        while (context.vars.hasNextimageid === false && context.vars.postlistimages.length > 0) {
            context.vars.nextimageid = context.vars.postlistimages.splice(-1, 1)[0]; // remove element from array
            context.vars.hasNextimageid = !context.vars.readimages.has(context.vars.nextimageid)
        }
}

function hasMoreInImageList(context, next) {
    checkHasMoreInImageList(context);
    return next(context.vars.hasNextimageid)
}

function setSearchPar(context, events, done) {
    context.vars.searchQuery = `${Faker.lorem.words()}`;

    if (Math.random() >= 0.5)
        context.vars.searchLocation = 'westeu';
    else {
        context.vars.searchLocation = 'westus';
    }
    return done();
}


/**
 * Process reply of the new post.
 * Stores in the array "postIds" the identifier and community of the uploaded post.
 */
function genNewPostReply(requestParams, response, context, ee, next) {

    if (response.body && response.body.length > 0 && response.statusCode === 200) {
        postIds.push([response.body, context.vars.community]);
        myPostIds.push(response.body);
        fs.writeFileSync('postids.data', JSON.stringify(myPostIds));
    }
    return next()
}

/**
 * Sets the body to an image, when using images.
 */
function setNewPostImageBody(requestParams, context, ee, next) {
    if (context.vars.hasImage) {
        requestParams.body = context.vars.imageBytes
    }
    return next()
}

/**
 * Process reply of the upload of an image.
 * Stores in the variable "imageId" the identifier of the uploaded image.
 */
function genNewImageReply(requestParams, response, context, ee, next) {
    if (response.body && response.body.length > 0) {
        context.vars.imageId = response.body
    }
    return next()
}

/**
 * Process reply of the download of an image.
 * Update the next image to read.
 */
function getImageReply(requestParams, response, context, ee, next) {
    if (typeof response.body !== 'undefined' && response.body.length > 0) {
        context.vars.readimages.add(context.vars.nextimageid)
    }
    checkHasMoreInImageList(context);
    return next()
}


/**
 * Function for initializing state regarding a browsing session.
 * The following variable will be used:
 * "idstoread" - arrays with post ids to read in the future
 * "imagesread" - set with the ids of images downladed (to simulate a cache for not reading the same images every time)
 * "browsecount" - number of operations executed
 * "sessionuser" - name of the user for this session
 */
function startBrowse(context, events, done) {
    context.vars.indexPage = 0;
    context.vars.idstoread = [];
    context.vars.readimages = new Set();
    context.vars.browsecount = 0;
    context.vars.sessionuser = userNames.sample();
    return done()
}

/**
 * Function that controls whether the session continues or not.
 * The session will continue if there is any post id to read and with a decreasing probability.
 */
function endBrowse(context, next) {
    const continueLooping = random(100) > context.vars.browsecount;
    return next(context.vars.idstoread.length > 0 && continueLooping)
}


/**
 * Parse a list of post and select a random number of posts to check next.
 * Also decide whether to like or reply to the current post
 * Select next post to read and store information in the following variables:
 * "curid" - identifier of the current post
 * "nextid" - id of the next post to browse
 * "hasNextid" - true/false whether there is any other post to browse next
 * "nextimageid" - id of the next image to download
 * "hasNextimageid" - true/false whether there is any other image to download next
 * "browsecount" - update information on the session size
 * "like" - true/false whether should like post
 * "reply" - true/false whether should reply to post
 */
function selectFromPostList(requestParams, response, context, ee, next) {
    if (response.body && response.body.length > 0 && response.statusCode === 200) {
        let resp = JSON.parse(response.body);

        if (resp.length > 0) {
            let num = random(resp.length / 2);
            let i;
            for (i = 0; i < num; i++) {
                let pp = resp.sample();
                context.vars.idstoread.push([pp.id, pp.community])
            }
            context.vars.postlistimages = [];
            for (i = 0; i < resp.length; i++) {
                if (resp[i].imageId !== undefined && resp[i].imageId !== 'undefined' && resp[i].imageId !== "null_value" && !context.vars.readimages.has(resp[i].imageId)) {
                    context.vars.postlistimages.push(resp[i].imageId)

                }

            }
            checkHasMoreInImageList(context)
        } else {
            context.vars.hasNextimageid = false
        }
    }
    delete context.vars.like;
    delete context.vars.reply;
    if (context.vars.idstoread.length > 0) {
        context.vars.curid = context.vars.nextid;
        context.vars.curcommunity = context.vars.nextid;
        updateBrowse(context);
    } else {
        context.vars.hasNextid = false
    }
    return next()
}

function updateBrowse(context) {
    let pp = context.vars.idstoread.splice(-1, 1)[0];
    // never undefined
    context.vars.nextid = pp[0];
    context.vars.nextcommunity = pp[1];
    context.vars.hasNextid = true;
    context.vars.browsecount++
}

/**
 * Parse a thread of post and select a random number of posts to check next.
 * Also decide whether to like or reply to the current post
 * Select next post to read and store information in the following variables:
 * "curid" - identifier of the current post
 * "nextid" - id of the next post to browse
 * "hasNextid" - true/false whether there is any other post to browse next
 * "nextimageid" - id of the next image to download
 * "hasNextimageid" - true/false whether there is any other image to download next
 * "browsecount" - update information on the session size
 * "like" - true/false whether should like post
 * "reply" - true/false whether should reply to post
 */
function selectFromPostThread(requestParams, response, context, ee, next) {
    if (response.body && response.body.length > 0) {
        let resp = JSON.parse(response.body);
        if (typeof resp !== 'undefined' && resp.childPosts.length > 0) {
            let num = random(resp.childPosts.length / 2);
            let i;
            for (i = 0; i < num; i++) {
                let pp = resp.childPosts.sample();
                context.vars.idstoread.push([pp.id, pp.community])
            }
            context.vars.postlistimages = [];
            if (resp.imageId !== undefined && resp.imageId !== 'undefined' && resp.imageId !== "null_value" && !context.vars.readimages.has(resp.imageId)) {
                context.vars.postlistimages.push(resp.imageId);
            }
//			for( i = 0; i < resp.posts.length; i++) {
//				if( resp.posts[i].image !== "" && ! context.vars.readimages.has(resp.posts[i].image))
//					context.vars.postlistimages.push(resp.posts[i].image)
//			}
            checkHasMoreInImageList(context)
        } else {
            context.vars.hasNextimageid = false
        }
        if (typeof resp !== 'undefined') {
            context.vars.curcommunity = resp.community
        } else {
            context.vars.curcommunity = communityNames.sample()
        }
        if (random(100) < 33) {
            context.vars.like = true
        } else
            delete context.vars.like;
        if (random(100) < 25) {
            context.vars.reply = true
        } else
            delete context.vars.reply
    } else {
        delete context.vars.like;
        delete context.vars.reply
    }
    if (context.vars.idstoread.length > 0) {
        context.vars.curid = context.vars.nextid;
        context.vars.curcommunity = context.vars.nextcommunity;

        context.vars.curcreationTime = Date.now();

        if (context.vars.curid === null || context.vars.curid === undefined) {
            context.vars.curtitle = `${Faker.lorem.words()}`
        }

        updateBrowse(context);
    } else {
        context.vars.curid = context.vars.nextid;
        context.vars.curcommunity = context.vars.nextcommunity;
        context.vars.hasNextid = false;

        context.vars.curcreationTime = Date.now();
        if (context.vars.curid === null || context.vars.curid === undefined) {
            context.vars.curtitle = `${Faker.lorem.words()}`
        }
    }
    return next()
}

/**
 * Parse a list of post and select all posts to check next.
 * Select next post to read and store information in the following variables:
 * "curid" - identifier of the current post
 * "nextid" - id of the next post to browse
 * "community" - community of the next post to browse
 * "hasNextid" - true/false whether there is any other post to browse next
 * "browsecount" - update information on the session size
 */
function selectAllFromPostList(requestParams, response, context, ee, next) {
    if (response.body && response.body.length > 0) {
        let resp = JSON.parse(response.body);
        let i;
        for (i = 0; i < resp.length; i++) {
            context.vars.idstoread.push([resp[i].id, resp[i].community])
        }
        context.vars.postlistimages = [];
        for (i = 0; i < resp.length; i++) {
            if (resp[i].imageId !== undefined && resp[i].imageId !== 'undefined' && resp[i].imageId !== "null_value" && !context.vars.readimages.has(resp[i].imageId))
                context.vars.postlistimages.push(resp[i].imageId)
        }
        checkHasMoreInImageList(context)
    }
    delete context.vars.like;
    delete context.vars.reply;
    stillToRead(context);
    return next()
}

function stillToRead(context) {
    if (context.vars.idstoread.length > 0) {
        updateBrowse(context);
    } else {
        context.vars.hasNextid = false
    }
}
