npm install -g artillery --unsafe-perm=true --allow-root
npm install -g faker
npm install -g node-fetch --save
npm install -g --save-dev artillery-plugin-metrics-by-endpoint
npm install -g https://github.com/preguica/artillery-plugin-metrics-by-endpoint.git

docker run -v "$(pwd)":/config -t nunopreguica/ccs1920-test artillery run create-posts.yml

# OR

npm install
npm install --save https://github.com/preguica/artillery-plugin-metrics-by-endpoint.git

# docker run -it --entrypoint /bin/sh -v $(pwd):/config nunopreguica/ccs-1920
# Run the benchmarks, saving results in a file in directory results
artillery run --output results general-test.yml
# Produce a report from the saved results
artillery report results/artillery_report_20191119_234619.json
# Run the benchmark with debug on
DEBUG=http artillery run general-test.yml



for i in results/*.json ; do artillery report results/"$i" ; done


az aks show --resource-group scc1920-cluster-12345 --name cluster-12345 --query nodeResourceGroup -o tsv MC_myResourceGroup_myAKSCluster_eastu

az disk create \
  --resource-group scc1920-cluster-12345 \
  --name configDisk \
  --size-gb 1 \
  --query id --output tsv

/subscriptions/3190f703-1ef5-49ed-a53c-ebb74a71992a/resourceGroups/scc1920-cluster-12345/providers/Microsoft.Compute/disks/configDisk

kubectl exec -it scc-tester-12345 -- /bin/sh