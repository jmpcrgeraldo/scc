#!/bin/sh

touch results/test.txt

echo "removing old files"
scripts/clean.sh
echo "filling database"
scripts/fill.sh
echo "running main test"
scripts/run.sh
echo "generating results"
scripts/res.sh
echo "done"
cp -a results/*.html /mnt/res/
echo "copy done"

top

# kubectl delete deployments,services,pods --all