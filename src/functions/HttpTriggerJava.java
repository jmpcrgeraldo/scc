package functions;

import com.microsoft.azure.cognitiveservices.vision.computervision.ComputerVisionClient;
import com.microsoft.azure.cognitiveservices.vision.computervision.ComputerVisionManager;
import com.microsoft.azure.cognitiveservices.vision.computervision.models.ComputerVisionErrorException;
import com.microsoft.azure.cognitiveservices.vision.computervision.models.ImageAnalysis;
import com.microsoft.azure.cognitiveservices.vision.computervision.models.ImageTag;
import com.microsoft.azure.cognitiveservices.vision.computervision.models.VisualFeatureTypes;
import com.microsoft.azure.functions.*;
import com.microsoft.azure.functions.annotation.AuthorizationLevel;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.HttpTrigger;
import scc.config.GeoLocation;
import scc.config.VisionAccessKeys;
import scc.resources.Post;
import scc.server.requests.MultimediaRequests;
import scc.utils.FieldTest;
import scc.utils.GenericRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Azure Functions with HTTP Trigger.
 */
public class HttpTriggerJava {

    @FunctionName("HttpTriggerJava")
    public HttpResponseMessage run(@HttpTrigger(name = "req", methods = {HttpMethod.GET}, authLevel = AuthorizationLevel.FUNCTION) HttpRequestMessage<Optional<String>> request, final ExecutionContext context) {
        /*
        GeoLocation.setLocation(GeoLocation.LOCATION_FUNC);

        context.getLogger().info("Java HTTP trigger is processing a request...");

        try {
            String query_post_id = request.getQueryParameters().get("post");
            String post_id = request.getBody().orElse(query_post_id);

            if (post_id == null) {
                return request.createResponseBuilder(HttpStatus.BAD_REQUEST).body("No post").build();
            }

            Post post = GenericRequest.get(Post.RESOURCE_NAME, post_id, Post.class);

            if (FieldTest.isEmpty(post.getImageId())) {
                return request.createResponseBuilder(HttpStatus.BAD_REQUEST).body("No image").build();
            }

            ComputerVisionClient compVisClient = ComputerVisionManager.authenticate(VisionAccessKeys.getInstance().getKey()).withEndpoint(VisionAccessKeys.getInstance().getURL());

            List<VisualFeatureTypes> featuresToExtractFromLocalImage = new ArrayList<>();
            featuresToExtractFromLocalImage.add(VisualFeatureTypes.TAGS);
            featuresToExtractFromLocalImage.add(VisualFeatureTypes.ADULT);

            byte[] imageByteArray = new MultimediaRequests().download(post.getImageId());

            ImageAnalysis analysis = compVisClient
                    .computerVision()
                    .analyzeImageInStream()
                    .withImage(imageByteArray)
                    .withVisualFeatures(featuresToExtractFromLocalImage)
                    .execute();

            List<String> tags = analysis.tags().stream().map(ImageTag::name).collect(Collectors.toList());
            boolean isNSFW = analysis.adult().isAdultContent();

            post.setTags(tags);
            post.setNsfw(isNSFW);
            GenericRequest.update(Post.RESOURCE_NAME, post);

            context.getLogger().info("Successfully got tags for post " + post_id);

            return request.createResponseBuilder(HttpStatus.OK).body("OK").build();
        } catch (ComputerVisionErrorException e) {
            // Error is thrown when the cap for the maximum number of requests per minute is reached
            if (e.getMessage().contains("429")) {
                // TODO a solution would be to add the post id to a queue and try again at a later date
                context.getLogger().warning("Unable to perform request, reach max queries per minute");

                return request.createResponseBuilder(HttpStatus.OK).body("DELAYED").build();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return request.createResponseBuilder(HttpStatus.BAD_REQUEST).body("Something bad happened...").build();
    
*/    
        return request.createResponseBuilder(HttpStatus.OK).body("OK").build();
    }
    
}


