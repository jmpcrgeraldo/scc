package functions;

import com.microsoft.azure.functions.ExecutionContext;
import com.microsoft.azure.functions.annotation.BindingName;
import com.microsoft.azure.functions.annotation.BlobTrigger;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import scc.config.AllBlobKeys;
import scc.config.BlobStorageAccessKeys;
import scc.config.GeoLocation;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;

/**
 * Azure Functions with Azure Blob trigger.
 */
public class BlobTriggerJava {

    /**
     * This function will be invoked when a new or updated blob is detected at the
     * specified path. The blob contents are provided as input to this function.
     */
    @FunctionName("BlobTriggerJava")
    public void run(
            @BlobTrigger(name = "content", path = "images/{name}", dataType = "binary", connection = "BlobStoreConnection") byte[] content,
            @BindingName("name") String name, final ExecutionContext context) {

        GeoLocation.setLocation(GeoLocation.LOCATION_FUNC);

        context.getLogger().info("Java Blob trigger function processed a blob. Name: " + name + "\n  Size: "
                + content.length + " Bytes");

        try {
            // When a blob is create is this region, the trigger function will create the same blob on all the other regions
            for (String blobKey : AllBlobKeys.getInstance().getBlobKeys()) {
                if (!blobKey.equals(BlobStorageAccessKeys.getInstance().getConnectionString())) {

                    CloudStorageAccount storageAccount = CloudStorageAccount.parse(blobKey);
                    CloudBlobClient client = storageAccount.createCloudBlobClient();
                    CloudBlobContainer container = client.getContainerReference("images");
                    CloudBlob blob = container.getBlockBlobReference(name);

                    if (blob.exists()) {
                        context.getLogger().info("Blob already exists, skipping");
                        continue;
                    }
                    blob.getProperties().setContentType("image/" + name.split("\\.")[1]);
                    blob.uploadFromByteArray(content, 0, content.length);
                    context.getLogger().info(String.format("Successfully uploaded blob (%s) to %d locations", name, AllBlobKeys.getInstance().getBlobKeys().size() - 1));
                }
            }
        } catch (URISyntaxException | InvalidKeyException | StorageException | IOException e) {
            context.getLogger().info(String.format("An error occurred when uploading blob (%s) -> (%s)", name, e.getMessage()));
        }
    }
}
