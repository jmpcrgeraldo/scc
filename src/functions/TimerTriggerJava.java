package functions;

import com.microsoft.azure.functions.ExecutionContext;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.TimerTrigger;
import scc.config.GeoLocation;
import scc.utils.FrontPageMaker;

import java.time.LocalDateTime;

/**
 * Azure Functions with Timer trigger.
 */
public class TimerTriggerJava {

    /**
     * This function will be invoked periodically according to the specified
     * schedule.
     */
    @FunctionName("TimerTriggerJava")
    public void run(@TimerTrigger(name = "timerInfo", schedule = "0 0 0 */2 * *") String timerInfo, final ExecutionContext context) {
/*
        GeoLocation.setLocation(GeoLocation.LOCATION_FUNC);

        context.getLogger().info("Java Timer trigger function executed at: " + LocalDateTime.now());

        // Create/Update Global and Local Front Pages
        FrontPageMaker.make(null);
        FrontPageMaker.make(GeoLocation.getInstance().getPhysicalLocation());

  
      context.getLogger().info("Done !!!!!!!!!!!!!!!!!!!!!!!!!!!: " + LocalDateTime.now());
*/
    }

}
