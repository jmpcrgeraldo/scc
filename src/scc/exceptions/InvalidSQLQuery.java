package scc.exceptions;

public class InvalidSQLQuery extends Exception {

    public InvalidSQLQuery() {

    }

    public InvalidSQLQuery(String errorMessage) {
        super(errorMessage);
    }

}