package scc.exceptions;

public class ResourceNotFound extends Exception {

    public ResourceNotFound() {

    }

    public ResourceNotFound(String errorMessage) {
        super(errorMessage);
    }

}