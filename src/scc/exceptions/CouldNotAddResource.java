package scc.exceptions;

public class CouldNotAddResource extends Exception {

    public CouldNotAddResource() {

    }

    public CouldNotAddResource(String errorMessage) {
        super(errorMessage);
    }

}