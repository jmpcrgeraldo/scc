package scc.exceptions;

public class InvalidParameters extends Exception {

    public InvalidParameters() {

    }

    public InvalidParameters(String errorMessage) {
        super(errorMessage);
    }

}