package scc.exceptions;

public class InvalidDBLink extends Exception {

    public InvalidDBLink() {

    }

    public InvalidDBLink(String errorMessage) {
        super(errorMessage);
    }

}