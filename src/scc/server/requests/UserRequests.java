package scc.server.requests;

import scc.exceptions.CouldNotAddResource;
import scc.exceptions.InvalidParameters;
import scc.exceptions.ResourceNotFound;
import scc.resources.User;
import scc.utils.GenericRequest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@Path("/user")
public class UserRequests {
    public static final String RESOURCE_NAME = User.RESOURCE_NAME;
    private static final Logger logger = Logger.getLogger(UserRequests.class.getName());

    public UserRequests() {
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addUser(User user) {
        logger.info(String.format("GOT -> USER : ADD (%s)", user.getName()));
        user.setID(user.getName());
        try {
            return GenericRequest.add(RESOURCE_NAME, user);
        } catch (InvalidParameters | CouldNotAddResource invalidParameters) {
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUser(@PathParam("id") String id) {
        logger.info(String.format("GOT -> USER : GET (%s)", id));
        try {
            return GenericRequest.get(RESOURCE_NAME, id, User.class);
        } catch (ResourceNotFound | InvalidParameters resourceNotFound) {
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUser(User user) {
        logger.info(String.format("GOT -> USER : UPDATE (%s)", user.getName()));
        try {
            GenericRequest.update(RESOURCE_NAME, user);
            return Response.ok().build();
        } catch (InvalidParameters | ResourceNotFound invalidParameters) {
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("id") String id) {
        logger.info(String.format("GOT -> USER : DELETE (%s)", id));
        try {
            GenericRequest.delete(RESOURCE_NAME, id);
            return Response.ok().build();
        } catch (ResourceNotFound | InvalidParameters resourceNotFound) {
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }
    }
}