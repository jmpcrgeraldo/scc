package scc.server.requests;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import scc.config.GeoLocation;
import scc.config.VisionAccessKeys;
import scc.exceptions.CouldNotAddResource;
import scc.exceptions.InvalidParameters;
import scc.exceptions.ResourceNotFound;
import scc.resources.Post;
import scc.resources.PostThread;
import scc.resources.User;
import scc.utils.CacheClient;
import scc.utils.DBClient;
import scc.utils.FieldTest;
import scc.utils.GenericRequest;

import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.logging.Logger;

@Path("/post")
public class PostRequests {
    private static final String RESOURCE_NAME = Post.RESOURCE_NAME;
    private static final Logger logger = Logger.getLogger(PostRequests.class.getName());

    public PostRequests() {
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addPost(Post post) {
        logger.info(String.format("GOT -> POST : ADD (%s)", post.getCreator()));

        try {
            // Check if user exists
            GenericRequest.get(User.RESOURCE_NAME, post.getCreator(), User.class);
        } catch (ResourceNotFound resourceNotFound) {
            logger.warning(String.format("GOT -> POST -> user (%s) does not exist", post.getCreator()));
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        } catch (InvalidParameters invalidParameters) {
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }

        if (post.getParentId() == null || post.getParentId().isEmpty()) {
            post.setParentId(DBClient.NULL_STRING);
        } else {
            try {
                // Check if parent exists
                GenericRequest.get(User.RESOURCE_NAME, post.getCreator(), User.class);
            } catch (ResourceNotFound resourceNotFound) {
                logger.warning(String.format("GOT -> POST -> parent (%s) does not exist", post.getCreator()));
                throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
            } catch (InvalidParameters invalidParameters) {
                throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
            }
            post.setTitle(DBClient.NULL_STRING);
        }


        if (post.getImageId() == null || post.getImageId().isEmpty()) {
            post.setImageId(DBClient.NULL_STRING);
        } else {
            String base_url = VisionAccessKeys.getInstance().getFuncUrl();
            /*new Thread(() -> {
                try {
                    Client client = new ResteasyClientBuilder().build();
                    WebTarget target = client.target(new URI(base_url + post.getID()));
                    Response response = target.request().get();
                    logger.info("GOT VISION REPLY -> " + response.getStatus());
                } catch (Exception e) {
                    // e.printStackTrace();
                    logger.info("FAILED VISION REPLY");
                }
            }).start();*/
        }

        post.setCreationTime(System.currentTimeMillis());
        post.setLocation(GeoLocation.getInstance().getPhysicalLocation());

        String id;
        try {
            id = GenericRequest.add(RESOURCE_NAME, post);
        } catch (InvalidParameters | CouldNotAddResource invalidParameters) {
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }

        if (!FieldTest.isEmpty(post.getParentId())) {
            CacheClient.addToList(PostThread.RESOURCE_NAME + post.getParentId(), post.getID());
        }

        return id;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Post getPost(@PathParam("id") String id) {
        logger.info(String.format("GOT -> POST : GET (%s)", id));

        try {
            return GenericRequest.get(RESOURCE_NAME, id, Post.class);
        } catch (ResourceNotFound resourceNotFound) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        } catch (InvalidParameters invalidParameters) {
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }
    }

    @PUT
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePost(Post post) {
        logger.info(String.format("GOT -> POST : UPDATE (%s)", post.getCreator()));
        try {
            GenericRequest.update(RESOURCE_NAME, post);
        } catch (InvalidParameters | ResourceNotFound invalidParameters) {
            //invalidParameters.printStackTrace();
            logger.warning(invalidParameters.getMessage());
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePost(@PathParam("id") String id) {
        logger.info(String.format("GOT -> POST : DELETE (%s)", id));
        try {
            GenericRequest.delete(RESOURCE_NAME, id);
        } catch (ResourceNotFound | InvalidParameters resourceNotFound) {
            logger.warning(resourceNotFound.getMessage());
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        return Response.ok().build();
    }
}
