package scc.server.requests;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import scc.config.BlobStorageAccessKeys;
import scc.utils.FileTypeParser;
import scc.utils.IDGenerator;
import scc.utils.Pair;

import javax.activation.UnsupportedDataTypeException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.logging.Logger;
import java.util.stream.StreamSupport;

@Path("/media")
public class MultimediaRequests {
    private static final String CONTAINER_NAME = "images";
    private static final Logger logger = Logger.getLogger(MultimediaRequests.class.getName());

    private CloudStorageAccount storageAccount;
    private CloudBlobClient client;

    public MultimediaRequests() {
        try {
            storageAccount = CloudStorageAccount.parse(BlobStorageAccessKeys.getInstance().getConnectionString());
            this.client = null;
        } catch (URISyntaxException | InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    private void checkConnection() {
        if (this.client == null)
            this.client = storageAccount.createCloudBlobClient();
    }

    @POST
    @Path("/{filename}")
    @Consumes(MediaType.APPLICATION_OCTET_STREAM)
    @Produces(MediaType.APPLICATION_JSON)
    public String upload(@PathParam("filename") String filename, byte[] contents) {
        logger.info(String.format("GOT -> FILE UPLOAD (%s) SIZE (%d)", filename, contents.length));
        this.checkConnection();

        try {
            String file_id = IDGenerator.get();
            Pair<String, String> result = FileTypeParser.parseType(filename, file_id);
            CloudBlobContainer container = client.getContainerReference(CONTAINER_NAME);
            CloudBlob blob = container.getBlockBlobReference(result.getSecond());

            if (blob.exists()) {
                return result.getSecond();
            }

            blob.getProperties().setContentType(result.getFirst());
            blob.uploadFromByteArray(contents, 0, contents.length);
            return result.getSecond();
        } catch (UnsupportedDataTypeException e) {
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).entity(String.format("File type not supported file (%s)", filename)).build());
        } catch (URISyntaxException | StorageException | IOException e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.status(Response.Status.NOT_ACCEPTABLE).entity(String.format("Unable to upload file (%s)", filename)).build());
        }
    }

    @GET
    @Path("/{file_id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public byte[] download(@PathParam("file_id") String file_id) {
        logger.info(String.format("GOT -> FILE DOWNLOAD (%s)", file_id));
        this.checkConnection();

        try {
            CloudBlobContainer container = client.getContainerReference(CONTAINER_NAME);
            CloudBlob blob = container.getBlobReferenceFromServer(file_id);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            blob.download(out);
            out.close();
            return out.toByteArray();
        } catch (URISyntaxException | StorageException | IOException e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.status(Response.Status.NOT_ACCEPTABLE).entity(String.format("Unable to retrieve file (%s)", file_id)).build());
        }
    }

    @DELETE
    @Path("/{file_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("file_id") String file_id) {
        logger.info(String.format("GOT -> FILE DELETE (%s)", file_id));
        this.checkConnection();

        try {
            CloudBlobContainer container = client.getContainerReference(CONTAINER_NAME);
            CloudBlob blob = container.getBlobReferenceFromServer(file_id);
            blob.delete();
        } catch (URISyntaxException | StorageException e) {
            throw new WebApplicationException(Response.status(Response.Status.NOT_ACCEPTABLE).entity(String.format("Unable to delete file (%s)", file_id)).build());
        }
    }

    public void deleteAll() {
        this.checkConnection();
        try {
            StreamSupport.stream(client.getContainerReference(CONTAINER_NAME)
                    .listBlobs()
                    .spliterator(), true)
                    .forEach(
                            b -> {
                                try {
                                    ((CloudBlob) b).delete();
                                } catch (StorageException ignored) {
                                }
                            }
                    );
        } catch (URISyntaxException | StorageException ignored) {
        }
    }
}
