package scc.server.requests;


import scc.exceptions.CouldNotAddResource;
import scc.exceptions.InvalidParameters;
import scc.exceptions.ResourceNotFound;
import scc.resources.Community;
import scc.utils.GenericRequest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@Path("/community")
public class CommunityRequests {
    private static final String RESOURCE_NAME = Community.RESOURCE_NAME;
    private static final Logger logger = Logger.getLogger(CommunityRequests.class.getName());

    public CommunityRequests() {
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addCommunity(Community community) {
        logger.info(String.format("GOT -> COMMUNITY : ADD (%s)", community.getName()));
        community.setID(community.getName());
        try {
            return GenericRequest.add(RESOURCE_NAME, community);
        } catch (InvalidParameters | CouldNotAddResource invalidParameters) {
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Community getCommunity(@PathParam("id") String id) {
        logger.info(String.format("GOT -> COMMUNITY : GET (%s)", id));
        try {
            return GenericRequest.get(RESOURCE_NAME, id, Community.class);
        } catch (ResourceNotFound | InvalidParameters resourceNotFound) {
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }
    }

    @PUT
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCommunity(Community community) {
        logger.info(String.format("GOT -> COMMUNITY : UPDATE (%s)", community.getName()));
        try {
            GenericRequest.update(RESOURCE_NAME, community);
            return Response.ok().build();
        } catch (InvalidParameters | ResourceNotFound invalidParameters) {
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }

    }

    @DELETE
    @Path("/{id}")
    public Response deleteCommunity(@PathParam("id") String id) {
        logger.info(String.format("GOT -> COMMUNITY : DELETE (%s)", id));
        try {
            GenericRequest.delete(RESOURCE_NAME, id);
            return Response.ok().build();
        } catch (ResourceNotFound | InvalidParameters resourceNotFound) {
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }
    }
}
