package scc.server.requests;


import scc.utils.CacheClient;
import scc.utils.DBRequest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@Path("/")
public class DebugRequests {
    private static final Logger logger = Logger.getLogger(DebugRequests.class.getName());

    public DebugRequests() {
    }

    @GET
    @Path("/v1")
    public Response ping() {
        logger.info("GOT -> PING");

        return Response.status(Response.Status.OK).build();
    }

    @GET
    @Path("/clear")
    public Response clear() {
        logger.info("GOT -> CLEAR");
        DBRequest.clearResources();
        CacheClient.clear();
        new MultimediaRequests().deleteAll();
        return Response.status(Response.Status.OK).build();
    }
}
