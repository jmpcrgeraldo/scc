package scc.server.requests;

import scc.config.GeoLocation;
import scc.exceptions.CouldNotAddResource;
import scc.exceptions.InvalidParameters;
import scc.exceptions.ResourceNotFound;
import scc.resources.Post;
import scc.resources.PostLikes;
import scc.resources.ResourceID;
import scc.resources.User;
import scc.utils.GenericRequest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

@Path("/post")
public class LikeRequest {
    private static final String RESOURCE_NAME = PostLikes.RESOURCE_NAME;

    private static final Logger logger = Logger.getLogger(PostLikes.class.getName());

    @POST
    @Path("/like/{post_id}/{user_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String giveLike(@PathParam("post_id") String post_id, @PathParam("user_id") String user_id) {

        try {
            // Check if user exists
            GenericRequest.get(User.RESOURCE_NAME, user_id, User.class);
        } catch (ResourceNotFound | InvalidParameters resourceNotFound) {
            logger.warning(String.format("GOT -> Like -> user (%s) does not exist", user_id));
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }

        try {
            PostLikes postLikes = GenericRequest.get(RESOURCE_NAME, post_id, PostLikes.class);

            if (postLikes.getUsernames().contains(user_id)) {
                logger.warning(String.format("GOT -> Like -> user (%s) already liked", user_id));
                return Integer.toString(1);
            }

            postLikes.getUsernames().add(user_id);
            postLikes.setLikeCount(postLikes.getLikeCount() + 1);

            GenericRequest.update(RESOURCE_NAME, postLikes);

            return Integer.toString(postLikes.getLikeCount());
        } catch (ResourceNotFound resourceNotFound) {

            try {
                Post post = GenericRequest.get(Post.RESOURCE_NAME, post_id, Post.class);

                PostLikes post_likes = new PostLikes();
                List<String> user_ids = new LinkedList<>();

                user_ids.add(user_id);
                post_likes.setID(post_id);
                post_likes.setUsernames(user_ids);
                post_likes.setLikeCount(1);
                post_likes.setPostDate(post.getCreationTime());
                post_likes.setPostLocation(GeoLocation.getInstance().getPhysicalLocation());

                GenericRequest.add(RESOURCE_NAME, post_likes);
            } catch (ResourceNotFound notFound) {
                logger.warning(String.format("GOT -> Like -> post (%s) does not exist", post_id));
                throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
            } catch (InvalidParameters | CouldNotAddResource invalidParameters) {
                throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
            }

            return Integer.toString(1);
        } catch (InvalidParameters invalidParameters) {
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }
    }

    @POST
    @Path("/unlike/{post_id}/{user_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String takeLike(@PathParam("post_id") String post_id, @PathParam("user_id") String user_id) {

        try {
            // Check if user exists
            try {
                GenericRequest.get(User.RESOURCE_NAME, user_id, User.class);
            } catch (InvalidParameters invalidParameters) {
                throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
            }

            PostLikes postLikes = null;
            try {
                postLikes = GenericRequest.get(RESOURCE_NAME, post_id, PostLikes.class);
            } catch (InvalidParameters invalidParameters) {
                throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
            }

            boolean removed = postLikes.getUsernames().remove(user_id);
            int like_count = postLikes.getLikeCount();

            if (removed)
                postLikes.setLikeCount(--like_count);

            try {
                GenericRequest.update(RESOURCE_NAME, postLikes);
            } catch (InvalidParameters invalidParameters) {
                throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
            }

            return Integer.toString(like_count);
        } catch (ResourceNotFound resourceNotFound) {
            logger.warning(String.format("GOT -> Like -> post or user (%s) (%s) does not exist", post_id, user_id));
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }
    }
}
