package scc.server.requests;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import scc.config.GeoLocation;
import scc.config.SearchAccessKeys;
import scc.resources.Search;
import scc.utils.FieldTest;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.StringJoiner;
import java.util.logging.Logger;

@Path("/search")
public class SearchRequest {
    private static final Logger logger = Logger.getLogger(SearchRequest.class.getName());

    public SearchRequest() {
    }

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public String search(Search search) {
        logger.info("GOT -> SEARCH");

        try {
            String hostname = "https://" + SearchAccessKeys.getInstance().getName() + ".search.windows.net/";
            Client client = new ResteasyClientBuilder().build();
            URI baseURI = UriBuilder.fromUri(hostname).build();
            WebTarget target = client.target(baseURI);
            String index = "cosmosdb-index";

            JsonObject obj = new JsonObject();
            obj.addProperty("count", "true");

            if (FieldTest.isEmpty(search.getQuery())) {
                logger.warning("Empty query field");
                throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
            }

            obj.addProperty("search", "+" + search.getQuery());
            obj.addProperty("searchFields", "msg,title,tags");

            StringJoiner joiner = new StringJoiner(" AND ");
            if (!FieldTest.isEmpty(search.getCreator())) {
                joiner.add("creator eq '" + search.getCreator() + "'");
            }
            if (!FieldTest.isEmpty(search.getCommunity())) {
                joiner.add("community eq '" + search.getCommunity() + "'");
            }
            if (!FieldTest.isEmpty(search.getLocation())) {
                joiner.add("location eq '" + search.getLocation() + "'");
            }

            obj.addProperty("filter", joiner.toString());
            obj.addProperty("searchMode", "all");
            obj.addProperty("queryType", "full");

            String resultStr = target
                    .path("indexes/" + index + "/docs/search")
                    .queryParam("api-version", "2019-05-06")
                    .request().header("api-key", SearchAccessKeys.getInstance().getKey())
                    .accept(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(obj.toString(), MediaType.APPLICATION_JSON))
                    .readEntity(String.class);

            JsonObject resultObj = new Gson().fromJson(resultStr, JsonObject.class);

            if (resultObj.get("@odata.count") == null) {
                logger.warning("Malformed query: " + resultStr);
                return "['error']";
            }

            logger.info("Number of results : " + resultObj.get("@odata.count").getAsInt());

            return resultObj.get("value").getAsJsonArray().toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "['error']";
        }
    }

}