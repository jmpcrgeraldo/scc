package scc.server;

import scc.config.GeoLocation;
import scc.server.pages.InitialPage;
import scc.server.pages.ThreadPage;
import scc.server.requests.*;

import javax.servlet.ServletContext;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/")
public class MainApplication extends Application {
    @Context ServletContext context;

    @Override
    public Set<Class<?>> getClasses() {

        // GeoLocation.setLocation(GeoLocation.LOCATION);
        GeoLocation.loadContext(context);

        Set<Class<?>> set = new HashSet<>();
        set.add(DebugRequests.class);
        set.add(MultimediaRequests.class);
        set.add(UserRequests.class);
        set.add(CommunityRequests.class);
        set.add(PostRequests.class);
        set.add(InitialPage.class);
        set.add(ThreadPage.class);
        set.add(LikeRequest.class);
        set.add(SearchRequest.class);
        return set;
    }


}