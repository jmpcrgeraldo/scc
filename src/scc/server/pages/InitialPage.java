package scc.server.pages;

import com.google.gson.Gson;
import scc.config.GeoLocation;
import scc.exceptions.CouldNotAddResource;
import scc.exceptions.InvalidParameters;
import scc.exceptions.ResourceNotFound;
import scc.server.requests.MultimediaRequests;
import scc.utils.CacheClient;
import scc.utils.FrontPageMaker;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.logging.Logger;

import scc.utils.GenericRequest;
import scc.resources.*;

@Path("/pages")
public class InitialPage {
    private static final Gson g = new Gson();
    private static Logger logger = Logger.getLogger(InitialPage.class.getName());

    private static String content = null;

    public static void main(String[] args) throws ResourceNotFound, InvalidParameters {

        GeoLocation.setLocation("props/");

        String c = "[{\"title\":\"null_value\",\"community\":\"voluptatemdicta\",\"creator\":\"Brenna.Funk\",\"creationTime\":1574891920176,\"msg\":\"Expedita consectetur tempore et animi. Iusto quia et id tempore mollitia. Et tempora aut veritatis laborum et ut autem voluptatibus. Adipisci totam fugiat magnam aspernatur et.\",\"imageId\":\"null_value\",\"parentId\":\"6b009b81890b49ca\",\"location\":\"westus\",\"id\":\"f7367042421c4ea8\",\"likeCount\":1},{\"title\":\"null_value\",\"community\":\"voluptatemdicta\",\"creator\":\"Hudson.Nicolas\",\"creationTime\":1574891921272,\"msg\":\"Neque nihil et aliquid velit harum. Aut consequuntur aliquid neque dicta maiores perspiciatis. Dolor in voluptatum. Quia molestiae voluptas qui doloribus est explicabo eligendi deleniti.\",\"imageId\":\"null_value\",\"parentId\":\"6b009b81890b49ca\",\"location\":\"westus\",\"id\":\"7e41442751ce408f\",\"likeCount\":2},{\"title\":\"null_value\",\"community\":\"voluptatemdicta\",\"creator\":\"Palma.Tremblay\",\"creationTime\":1574891926117,\"msg\":\"Illum consequatur optio autem id debitis. Ut voluptates sit quia. Tempore doloremque vel consequatur aliquam aut qui recusandae quis. Vel quia distinctio quidem similique provident et ad qui cupiditate. Veritatis ullam optio unde voluptatem. Eum ipsa aut et rerum consequatur.\",\"imageId\":\"6aa6512589c64e3d.jpeg\",\"parentId\":\"722de572885349f5\",\"location\":\"westus\",\"id\":\"b5f09db4656e4628\",\"likeCount\":2},{\"title\":\"non possimus ut\",\"community\":\"aliquidalias\",\"creator\":\"Destini.Sawayn\",\"creationTime\":1574891926479,\"msg\":\"Magni est laboriosam ut doloremque doloremque. Inventore a recusandae architecto numquam voluptates consequatur non. Odio et sit ipsum et. Velit vel rerum sequi est dolor molestias odit accusantium.\",\"imageId\":\"6aa6512589c64e3d.jpeg\",\"parentId\":\"null_value\",\"location\":\"westus\",\"id\":\"05df7f7ef68a4033\",\"likeCount\":1},{\"title\":\"null_value\",\"community\":\"dictarerum\",\"creator\":\"Deontae.Schinner\",\"creationTime\":1574891927373,\"msg\":\"Sint repellat repellat. Sint ut et eius inventore ut cum ut. Praesentium eius mollitia tempora et omnis ea fugit. Beatae quisquam nemo nulla illum qui. Quis magnam reprehenderit earum eos repellendus est libero.\",\"imageId\":\"ba10109a855f47f2.jpeg\",\"parentId\":\"467a83770f924383\",\"location\":\"westus\",\"id\":\"f4aacf2c9ccd4776\",\"likeCount\":1},{\"title\":\"null_value\",\"community\":\"repellatenim\",\"creator\":\"Rickey.Raynor\",\"creationTime\":1574891927939,\"msg\":\"Dolores et dignissimos. Sit alias harum et voluptas neque. Quam ad praesentium at dolor eum cupiditate. Iure veritatis omnis sapiente. Perferendis repudiandae est expedita exercitationem vel est facilis illum. Eos voluptatem labore et.\",\"imageId\":\"4cfd1a769b5442b8.jpeg\",\"parentId\":\"0175659abc804683\",\"location\":\"westus\",\"id\":\"03e004cd31b3471c\",\"likeCount\":1},{\"title\":\"null_value\",\"community\":\"repellatenim\",\"creator\":\"Kristy.Reichel\",\"creationTime\":1574891928855,\"msg\":\"Provident est libero. Et dolorum et voluptatum quisquam nisi sed eveniet at ut. Ipsum provident sequi porro.\",\"imageId\":\"3b15476680f34d85.jpeg\",\"parentId\":\"e4d43028318745da\",\"location\":\"westus\",\"id\":\"c8ad6ec2afc24beb\",\"likeCount\":1},{\"title\":\"null_value\",\"community\":\"dictarerum\",\"creator\":\"Jaron.Zboncak\",\"creationTime\":1574891929415,\"msg\":\"Voluptatem quaerat est minus. Eos numquam iusto explicabo aut ratione repellendus illo. Voluptatem dolorem repellat consequuntur temporibus quia omnis labore. Quibusdam quia similique. Molestiae numquam nemo qui repudiandae explicabo et incidunt excepturi. Qui non laborum omnis dolor vel dolorum earum id.\",\"imageId\":\"2744fe1066a54ae3.jpeg\",\"parentId\":\"f4aacf2c9ccd4776\",\"location\":\"westus\",\"id\":\"110ede77417849b4\",\"likeCount\":1},{\"title\":\"null_value\",\"community\":\"repellatenim\",\"creator\":\"Maximus.Botsford\",\"creationTime\":1574891930520,\"msg\":\"Necessitatibus voluptate eum minus. Voluptas eos neque officiis est tempora. Labore autem numquam aliquid ex nihil. Enim incidunt aut eveniet deleniti voluptate quidem aut laborum neque.\",\"imageId\":\"2744fe1066a54ae3.jpeg\",\"parentId\":\"e4d43028318745da\",\"location\":\"westus\",\"id\":\"b2dcde04c82249a3\",\"likeCount\":1},{\"title\":\"atque quis corporis\",\"community\":\"doloreius\",\"creator\":\"Florence.Mitchell\",\"creationTime\":1574891932136,\"msg\":\"Tempora est voluptatem magnam repellendus. Non rerum ad. Laudantium quam blanditiis est cum.\",\"imageId\":\"2744fe1066a54ae3.jpeg\",\"parentId\":\"null_value\",\"location\":\"westus\",\"id\":\"442acd64626e4cf1\",\"likeCount\":2},{\"title\":\"null_value\",\"community\":\"voluptatemdicta\",\"creator\":\"Orpha.Becker\",\"creationTime\":1574891932493,\"msg\":\"Adipisci molestiae laborum quis et nihil qui voluptas temporibus sint. Ullam in id iste quis sunt. Pariatur atque ea aut voluptatibus assumenda autem.\",\"imageId\":\"2744fe1066a54ae3.jpeg\",\"parentId\":\"ba155ecefdf842e0\",\"location\":\"westus\",\"id\":\"ca61dfca40734e0a\",\"likeCount\":1},{\"title\":\"null_value\",\"community\":\"aliquidalias\",\"creator\":\"Abraham.Marvin\",\"creationTime\":1574891935502,\"msg\":\"Quas totam totam et fuga consequuntur repudiandae. Voluptatibus beatae ut et aliquid et voluptatibus. Est ut impedit ut enim tenetur iure velit quas.\",\"imageId\":\"1d8efe4a6b35425a.jpeg\",\"parentId\":\"05bd3eca01d349fe\",\"location\":\"westus\",\"id\":\"1d24c7cc6f8c4888\",\"likeCount\":1},{\"title\":\"null_value\",\"community\":\"quolaborum\",\"creator\":\"Kristy.Reichel\",\"creationTime\":1574891935877,\"msg\":\"Eius sed eaque et placeat eveniet impedit. Magnam recusandae nulla consequatur velit. Et provident incidunt consequatur qui.\",\"imageId\":\"1d8efe4a6b35425a.jpeg\",\"parentId\":\"00cec2b613f74837\",\"location\":\"westus\",\"id\":\"2b3d50a8e3f740e8\",\"likeCount\":1},{\"title\":\"null_value\",\"community\":\"quiiure\",\"creator\":\"Florian.Farrell\",\"creationTime\":1574891936243,\"msg\":\"Nesciunt nihil ipsa explicabo inventore et at illo repellendus. Quia magnam sint voluptas nulla. Tempore saepe voluptates perferendis enim. Velit facilis velit vel quia culpa.\",\"imageId\":\"1d8efe4a6b35425a.jpeg\",\"parentId\":\"f6758ab9006a4b10\",\"location\":\"westus\",\"id\":\"2cebaa9fcabf44fe\",\"likeCount\":1},{\"title\":\"voluptatem eos totam\",\"community\":\"maximerecusandae\",\"creator\":\"Jaren.Pfeffer\",\"creationTime\":1574891936969,\"msg\":\"Ducimus reiciendis rerum et animi eum. Illum ducimus qui rerum dolore fuga et rem. Laboriosam omnis vel non rem sed harum et. Mollitia aut minima similique voluptatum magni inventore. Voluptas dignissimos id commodi.\",\"imageId\":\"1d8efe4a6b35425a.jpeg\",\"parentId\":\"null_value\",\"location\":\"westus\",\"id\":\"3b65f2b7e76b43a1\",\"likeCount\":1},{\"title\":\"null_value\",\"community\":\"dictarerum\",\"creator\":\"Marquis.Hahn\",\"creationTime\":1574891937426,\"msg\":\"Harum deleniti sequi eaque. Ab alias quo autem distinctio quisquam. Officiis ab id est maxime aut blanditiis. Velit et neque voluptas quia facilis.\",\"imageId\":\"1d8efe4a6b35425a.jpeg\",\"parentId\":\"110ede77417849b4\",\"location\":\"westus\",\"id\":\"fc047b324dd04d1a\",\"likeCount\":1},{\"title\":\"null_value\",\"community\":\"repellatenim\",\"creator\":\"Cecelia.Reilly\",\"creationTime\":1574891938128,\"msg\":\"Nemo laudantium et repellat quibusdam assumenda. Consequuntur eos quia rerum ut sunt. Suscipit ipsa iste earum perferendis dignissimos. Non modi similique consequatur quod. Iste et officiis voluptatem. Possimus sit aspernatur molestiae iure laborum.\",\"imageId\":\"1d8efe4a6b35425a.jpeg\",\"parentId\":\"210558458d1b4ca5\",\"location\":\"westus\",\"id\":\"73ec7e4ec08946bb\",\"likeCount\":1},{\"title\":\"null_value\",\"community\":\"doloreius\",\"creator\":\"Russ.Carter\",\"creationTime\":1574891939789,\"msg\":\"Doloribus sunt quos quis sit quisquam eum labore aut animi. Reiciendis sapiente voluptatem aperiam recusandae est deserunt iure autem. Magnam totam fugit debitis perferendis non nisi saepe sint quibusdam. Qui ut eum qui. Et ea voluptatem et rerum blanditiis ipsam omnis iste.\",\"imageId\":\"15bcd00ce2e1497d.jpeg\",\"parentId\":\"442acd64626e4cf1\",\"location\":\"westus\",\"id\":\"4c067dc87e104196\",\"likeCount\":1},{\"title\":\"eius tempora beatae\",\"community\":\"errorquo\",\"creator\":\"Oswald.Lebsack\",\"creationTime\":1574891940567,\"msg\":\"Neque placeat sequi sint molestiae voluptatem provident ipsam aut. Non nemo qui quo quia doloremque molestias aliquid sit occaecati. In earum natus ducimus sed inventore distinctio temporibus omnis.\",\"imageId\":\"15bcd00ce2e1497d.jpeg\",\"parentId\":\"null_value\",\"location\":\"westus\",\"id\":\"e9ca80187a4a4410\",\"likeCount\":1},{\"title\":\"null_value\",\"community\":\"voluptatemdicta\",\"creator\":\"Jerome.Douglas\",\"creationTime\":1574891942903,\"msg\":\"Temporibus aliquid quas illum voluptatum sapiente earum totam. Vel mollitia et maiores eos laborum possimus suscipit laboriosam. Molestiae harum atque sint laudantium nihil atque eligendi ullam eius. Consequatur autem rerum. Similique vero facilis dolorem sequi eos et id quidem. Autem quam eius rem laudantium vitae omnis.\",\"imageId\":\"a08981db201742aa.jpeg\",\"parentId\":\"6530e76f67074c0b\",\"location\":\"westus\",\"id\":\"a375137d4d654a15\",\"likeCount\":2},{\"title\":\"null_value\",\"community\":\"voluptatemdicta\",\"creator\":\"Ebony.Nitzsche\",\"creationTime\":1574891943270,\"msg\":\"Itaque ab ipsum ipsum corporis. Quidem quisquam tenetur possimus accusantium. Hic voluptas minima quia.\",\"imageId\":\"a08981db201742aa.jpeg\",\"parentId\":\"ba155ecefdf842e0\",\"location\":\"westus\",\"id\":\"24406ddbcb0a4153\",\"likeCount\":1},{\"title\":\"quis explicabo ipsum\",\"community\":\"aperiamquod\",\"creator\":\"Kristy.Reichel\",\"creationTime\":1574891947386,\"msg\":\"Est veritatis eum in mollitia laudantium facere distinctio. Modi aut quia deleniti excepturi provident autem non. Accusamus sequi ratione. Earum culpa ipsum voluptas eos ipsam aut. Aut dignissimos sunt molestias alias. Eum blanditiis qui eligendi omnis dolor est nisi.\",\"imageId\":\"169f03d4a84a4002.jpeg\",\"parentId\":\"null_value\",\"location\":\"westus\",\"id\":\"3bbb20930cd2497b\",\"likeCount\":2},{\"title\":\"totam eaque fugiat\",\"community\":\"eumexcepturi\",\"creator\":\"Abe.Rowe\",\"creationTime\":1574891948620,\"msg\":\"Quod amet consequatur animi omnis quibusdam quos. Assumenda vel eveniet iusto repellat quidem. Qui et et quam harum ex vero aut vero harum. Reprehenderit et facilis ut itaque et. Omnis temporibus et nisi.\",\"imageId\":\"f319fcfd379b4f51.jpeg\",\"parentId\":\"null_value\",\"location\":\"westus\",\"id\":\"ced8bcc7e3a0457b\",\"likeCount\":1},{\"title\":\"sit adipisci sapiente\",\"community\":\"etpariatur\",\"creator\":\"Candida.Hessel\",\"creationTime\":1574891949173,\"msg\":\"Aspernatur molestiae atque ut et. Est in et. Sed placeat non qui quia vel est quia nulla. Ex maxime rerum saepe. Eum quisquam est laborum vel. Sint nam necessitatibus iure nemo qui asperiores.\",\"imageId\":\"41cb0fa479254542.jpeg\",\"parentId\":\"null_value\",\"location\":\"westus\",\"id\":\"b9c86771746e47fa\",\"likeCount\":1},{\"title\":\"qui dolor similique\",\"community\":\"repellatenim\",\"creator\":\"Bobbie.Considine\",\"creationTime\":1574891949935,\"msg\":\"Id temporibus molestiae deserunt saepe reprehenderit rerum. Molestias expedita itaque voluptatem dolor quos. Dolores est pariatur iste id autem quasi quis ab earum. Et eveniet reiciendis qui blanditiis aut odio. Eum incidunt nam et ut voluptatem consectetur illum fugit placeat. Quia cumque quisquam ut alias autem animi non optio.\",\"imageId\":\"6c05acb8c1634163.jpeg\",\"parentId\":\"null_value\",\"location\":\"westus\",\"id\":\"c434ba36a7e54783\",\"likeCount\":2}]\n";
       // CacheClient.put("front_page_0", c);
        //System.out.println(new InitialPage().getPage("0"));
        //System.out.println(GenericRequest.get("front_page_", "0", Front.class));
        new MultimediaRequests().download("test");
    }

    @GET
    @Path("/all/{index}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getPage(@PathParam("index") String index) {

        // TODO change
        try {
            Front f = GenericRequest.get("front_page_", "0", Front.class);
            if(f == null) {
                logger.warning("Using DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD --------------------------------");
                String frontPage = FrontPageMaker.make(null);
                f = new Front();
                f.setContent(frontPage);
                f.setID("0");


                try {
                    GenericRequest.add("front_page_", f);
                } catch (InvalidParameters invalidParameters) {
                    invalidParameters.printStackTrace();
                } catch (CouldNotAddResource couldNotAddResource) {
                    couldNotAddResource.printStackTrace();
                }


                return f.getContent();
            }
            return f.getContent();
        } catch (Exception e) {
            logger.warning("Using DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD --------------------------------");
            String frontPage = FrontPageMaker.make(null);
            Front f = new Front();
            f.setContent(frontPage);
            f.setID("0");

            try {
                GenericRequest.add("front_page_", f);
            } catch (InvalidParameters invalidParameters) {
                invalidParameters.printStackTrace();
            } catch (CouldNotAddResource couldNotAddResource) {
                couldNotAddResource.printStackTrace();
            }

            return f.getContent();
        }
    }

    @GET
    @Path("/loc/{index}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getLocationPage(@PathParam("index") String index) {

        String loc = GeoLocation.getInstance().getPhysicalLocation();

        String locationPage = CacheClient.get(FrontPageMaker.FRONT_PAGE_PREFIX_KEY + loc + index);

        if (locationPage == null) {
            locationPage = FrontPageMaker.make(loc);
        }

        return locationPage == null ? "[]" : locationPage;
    }
}
