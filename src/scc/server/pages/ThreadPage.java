package scc.server.pages;

import com.google.gson.Gson;
import com.microsoft.azure.cosmosdb.Document;
import com.microsoft.azure.cosmosdb.FeedOptions;
import com.microsoft.azure.cosmosdb.FeedResponse;
import com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient;
import scc.exceptions.InvalidParameters;
import scc.exceptions.InvalidSQLQuery;
import scc.exceptions.ResourceNotFound;
import scc.resources.Post;
import scc.resources.PostThread;
import scc.utils.CacheClient;
import scc.utils.DBClient;
import scc.utils.GenericRequest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringJoiner;
import java.util.logging.Logger;

@Path("/pages/thread")
public class ThreadPage {
    public static final String CACHE_NAME = "postThread";
    private static Logger logger = Logger.getLogger(ThreadPage.class.getName());
    private static Gson g = new Gson();

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getPage(@PathParam("id") String id) {

        Post rootPost = null;
        try {
            rootPost = GenericRequest.get(Post.RESOURCE_NAME, id, Post.class);
        } catch (ResourceNotFound | InvalidParameters resourceNotFound) {
            logger.warning("Post does not exist: " + id);
            throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
        }

        List<String> childrenIds = CacheClient.getList(PostThread.RESOURCE_NAME + id);
        List<String> forDB = new LinkedList<>();

        List<Post> childPosts = new LinkedList<>();

        if (childrenIds != null && !childrenIds.isEmpty()) {
            Post p = null;
            for (String postId : childrenIds) {
                try {
                    //TODO optimize by executing a Query -> WHERE Post.id IN childrenIds, then check missing posts
                    p = GenericRequest.get(Post.RESOURCE_NAME, postId, Post.class, false);
                } catch (ResourceNotFound | InvalidParameters resourceNotFound) {
                    logger.warning("Post does not exist: " + postId);
                }
                if (p == null) {
                    forDB.add(postId);
                } else {
                    childPosts.add(p);
                }
            }
        } else {
            try {
                childPosts = GenericRequest.gets(Post.RESOURCE_NAME, Post.PARENT_ID_FIELD_NAME, id, Post.class);
            } catch (InvalidSQLQuery | InvalidParameters invalidSQLQuery) {
                logger.warning("Invalid parameters: " + id);
                throw new WebApplicationException(Response.Status.PRECONDITION_FAILED);
            }
            childPosts.forEach(p -> {
                CacheClient.put(p.getID(), g.toJson(p));
                CacheClient.addToList(PostThread.RESOURCE_NAME + id, p.getID());
            });
        }

        if (!forDB.isEmpty()) {
            try {
                StringJoiner join = new StringJoiner(", ");
                for (String postID : forDB) {
                    join.add("'" + postID + "'");
                }
                AsyncDocumentClient client = DBClient.get();
                FeedOptions queryOptions = new FeedOptions();
                queryOptions.setEnableCrossPartitionQuery(true);
                queryOptions.setMaxDegreeOfParallelism(-1);
                queryOptions.setMaxItemCount(250);

                String collection = DBClient.getCollectionString(Post.RESOURCE_NAME);
                String query = String.format("SELECT * FROM Posts p WHERE p.id IN (%s)", join.toString());
                Iterator<FeedResponse<Document>> it = client.queryDocuments(collection, query, queryOptions).toBlocking().getIterator();
                while (it.hasNext()) {
                    for (Document d : it.next().getResults()) {
                        Post elm = g.fromJson(d.toJson(), Post.class);
                        childPosts.add(elm);
                        CacheClient.put(elm.getID(), g.toJson(elm));
                    }
                }
            } catch (Exception e) {
                logger.warning("Post has no children: " + id);
            }
        }

        rootPost.setChildPosts(childPosts);
        return g.toJson(rootPost);
    }
}
