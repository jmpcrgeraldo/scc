package scc.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class SearchAccessKeys {
    public static final String SEARCH_PROP_FILE = "azure-search.props";
    public static final String PROP_SERVICE_NAME = "SearchServiceName";
    public static final String PROP_QUERY_KEY = "SearchServiceQueryKey";
    private static SearchAccessKeys instance;
    private final String search_name;
    private final String search_key;

    private SearchAccessKeys() {
        Properties prop = new Properties();
        String property_file = GeoLocation.getInstance().getLocation() + SEARCH_PROP_FILE;

        try (FileInputStream fis = new FileInputStream(property_file)) {
            prop.load(fis);
        } catch (IOException e) {
            System.err.println("Error reading property file in blob storage");
            e.printStackTrace();
        }

        search_name = prop.getProperty(PROP_SERVICE_NAME);
        search_key = prop.getProperty(PROP_QUERY_KEY);
    }

    public static SearchAccessKeys getInstance() {

        if (instance == null)
            instance = new SearchAccessKeys();

        return instance;
    }

    public String getName() {
        return search_name;
    }

    public String getKey() {
        return search_key;
    }


}
