package scc.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class VisionAccessKeys {
    public static final String SEARCH_PROP_FILE = "azure-vision.props";
    public static final String KEY = "KEY";
    public static final String URL = "URL";
    public static final String FUNCTION = "FUNCTION";
    private static VisionAccessKeys instance;
    private final String vision_key;
    private final String vision_url;
    private final String function_url;

    private VisionAccessKeys() {
        Properties prop = new Properties();
        String property_file = GeoLocation.getInstance().getLocation() + SEARCH_PROP_FILE;

        try (FileInputStream fis = new FileInputStream(property_file)) {
            prop.load(fis);
        } catch (IOException e) {
            System.err.println("Error reading property file in blob storage");
            e.printStackTrace();
        }

        vision_url = prop.getProperty(URL);
        vision_key = prop.getProperty(KEY);
        function_url = prop.getProperty(FUNCTION);
    }

    public static VisionAccessKeys getInstance() {

        if (instance == null)
            instance = new VisionAccessKeys();

        return instance;
    }

    public String getURL() {
        return vision_url;
    }

    public String getKey() {
        return vision_key;
    }

    public String getFuncUrl() {
        return function_url;
    }
}
