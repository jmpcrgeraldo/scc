package scc.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class RedisAccessKeys {
    private static final String HOSTNAME_KEY = "REDIS_URL";
    private static final String CACHE_KEY = "REDIS_KEY";
    private static RedisAccessKeys instance = null;
    private final String redis_hostname;
    private final String redis_cache_key;

    private RedisAccessKeys() {
        Properties prop = new Properties();
        String property_file = GeoLocation.getInstance().getPropsFile();

        try (FileInputStream fis = new FileInputStream(property_file)) {
            prop.load(fis);
        } catch (IOException e) {
            System.err.println("Error reading property file in blob storage");
            e.printStackTrace();
        }

        redis_hostname = prop.getProperty(HOSTNAME_KEY);
        redis_cache_key = prop.getProperty(CACHE_KEY);
    }

    public static RedisAccessKeys getInstance() {
        if (instance == null)
            instance = new RedisAccessKeys();
        return instance;
    }

    public String getHostname() {
        return redis_hostname;
    }

    public String getCacheKey() {
        return redis_cache_key;
    }
}
