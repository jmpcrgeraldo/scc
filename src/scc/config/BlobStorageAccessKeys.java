package scc.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class BlobStorageAccessKeys {
    private static final String KEY = "BLOB_KEY";
    private static BlobStorageAccessKeys instance = null;
    private final String connection_string;

    private BlobStorageAccessKeys() {
        Properties prop = new Properties();
        String property_file = GeoLocation.getInstance().getPropsFile();

        try (FileInputStream fis = new FileInputStream(property_file)) {
            prop.load(fis);
        } catch (IOException e) {
            System.err.println("Error reading property file in blob storage");
            e.printStackTrace();
        }

        connection_string = prop.getProperty(KEY);
    }

    public static BlobStorageAccessKeys getInstance() {
        if (instance == null)
            instance = new BlobStorageAccessKeys();
        return instance;
    }

    public String getConnectionString() {
        return connection_string;
    }

}