package scc.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class AllBlobKeys {

    private static final String KEY = "ALL_KEYS";
    private static AllBlobKeys instance = null;
    private final List<String> blobKeys;

    private AllBlobKeys() {
        Properties prop = new Properties();

        try (FileInputStream fis = new FileInputStream(GeoLocation.getInstance().getBlobKeysFile())) {
            prop.load(fis);
        } catch (IOException e) {
            System.err.println("Error reading property file in blob storage");
            e.printStackTrace();
        }

        blobKeys = new ArrayList<String>(Arrays.asList(prop.getProperty(KEY).split(" ")));
    }

    public static AllBlobKeys getInstance() {
        if (instance == null)
            instance = new AllBlobKeys();
        return instance;
    }

    public List<String> getBlobKeys() {
        return blobKeys;
    }
}
