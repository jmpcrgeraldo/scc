package scc.config;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.stream.Stream;

public class GeoLocation {
    public static final String LOCATION = null; // "../../../props/"; //"/home/site/wwwroot/webapps/ROOT/WEB-INF/props/";// "props/";
    public static final String LOCATION_FUNC = "D:\\home\\site\\wwwroot\\props\\";

    private static final String FILE = "geolocation.props";
    private static final String FILE_KEY = "FILE";
    private static final String PHYSICAL_LOCATION_KEY = "LOC";
    private static final String BLOB_KEYS_FILE = "azureblobs.props";

    private static String baseLocation;
    private static GeoLocation instance;
    private final String geo_location_props;
    private String physicalLocation;
    private static ServletContext context = null;

    private GeoLocation() {
        Properties prop = new Properties();

        if (context == null) {
            try (FileInputStream fis = new FileInputStream(baseLocation + FILE)) {
                prop.load(fis);
            } catch (IOException e) {
                System.err.println("Error reading location property file");
                e.printStackTrace();
            }
        } else {
            try {
                prop.load(context.getResourceAsStream("WEB-INF/props/" + FILE));
            } catch (IOException e) {
                System.err.println("Error reading location property file");
                e.printStackTrace();
            }
        }

        geo_location_props = prop.getProperty(FILE_KEY);
        physicalLocation = prop.getProperty(PHYSICAL_LOCATION_KEY);
    }

    public static GeoLocation getInstance() {

        if (instance == null)
            instance = new GeoLocation();

        return instance;
    }

    String getPropsFile() {
        return baseLocation + geo_location_props;
    }

    public String getPhysicalLocation() {
        return physicalLocation;
    }

    public String getBlobKeysFile() {
        return baseLocation + BLOB_KEYS_FILE;
    }

    String getLocation() {
        return baseLocation;
    }

    public static void setLocation(String loc) {
        if (loc != null) {
            baseLocation = loc;
            return;
        }
        System.out.println("----------------------------------------------------------");

        String start = "/opt/jboss/wildfly/standalone/tmp/vfs/temp/";
        File directoryPath = new File(start);
        start += directoryPath.list()[0] + "/";
        System.out.println(start);
        directoryPath = new File(start);
        start += directoryPath.list()[0] + "/";
        System.out.println(start);
        directoryPath = new File(start);
        start = directoryPath.getPath() + "/WEB-INF/props/";
        baseLocation = start;
        System.out.println(start);

        System.out.println("----------------------------------------------------------");
    }

    public static void loadContext(ServletContext c) {
        context = c;
        try {
            baseLocation = c.getResource("/").getPath() + "WEB-INF/props/";
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        System.err.println(":::::::: " +baseLocation);
    }
}
