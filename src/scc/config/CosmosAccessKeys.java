package scc.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class CosmosAccessKeys {
    private static final String ENDPOINT_KEY = "COSMOSDB_URL";
    private static final String MASTER_KEY = "COSMOSDB_KEY";
    private static final String DATABASE_KEY = "COSMOSDB_DATABASE";
    private static CosmosAccessKeys instance = null;
    private final String cosmos_db_endpoint;
    private final String cosmos_db_master_key;
    private final String cosmos_db_database;

    private CosmosAccessKeys() {
        Properties prop = new Properties();
        String property_file = GeoLocation.getInstance().getPropsFile();

        try (FileInputStream fis = new FileInputStream(property_file)) {
            prop.load(fis);
        } catch (IOException e) {
            System.err.println("Error reading property file in blob storage");
            e.printStackTrace();
        }

        cosmos_db_endpoint = prop.getProperty(ENDPOINT_KEY);
        cosmos_db_master_key = prop.getProperty(MASTER_KEY);
        cosmos_db_database = prop.getProperty(DATABASE_KEY);
    }

    public static CosmosAccessKeys getInstance() {
        if (instance == null)
            instance = new CosmosAccessKeys();
        return instance;
    }

    public String getEndpoint() {
        return cosmos_db_endpoint;
    }

    public String getMasterKey() {
        return cosmos_db_master_key;
    }

    public String getDatabase() {
        return cosmos_db_database;
    }

}
