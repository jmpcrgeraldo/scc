package scc.utils;

public class FieldTest {

    public static boolean isEmpty(String s) {
        return s == null || s.isEmpty() || s.equals(DBClient.NULL_STRING);
    }
}
