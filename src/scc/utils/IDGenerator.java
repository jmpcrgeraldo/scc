package scc.utils;

import java.util.UUID;

public class IDGenerator {

    public static String get() {
        return UUID.randomUUID().toString().replace("-", "").substring(0, 16);
    }
}
