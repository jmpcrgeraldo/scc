package scc.utils;

import com.microsoft.azure.cosmosdb.ConnectionPolicy;
import com.microsoft.azure.cosmosdb.ConsistencyLevel;
import com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient;
import scc.config.CosmosAccessKeys;
import scc.exceptions.InvalidSQLQuery;
import scc.resources.ResourceID;

public class DBClient {
    public static final String NULL_STRING = "null_value";

    private static final String advanced_sql_injection_protection = "^[a-zA-Z0-9_.@'-]+$";
    private static AsyncDocumentClient client = null;

    public static synchronized AsyncDocumentClient get() {
        if (client == null) {
            ConnectionPolicy connectionPolicy = ConnectionPolicy.GetDefault();
            client = new AsyncDocumentClient.Builder()
                    .withServiceEndpoint(CosmosAccessKeys.getInstance().getEndpoint())
                    .withMasterKeyOrResourceToken(CosmosAccessKeys.getInstance().getMasterKey())
                    .withConnectionPolicy(connectionPolicy)
                    .withConsistencyLevel(ConsistencyLevel.Eventual)
                    .build();
        }
        return client;
    }

    public static String getDBName() {
        return CosmosAccessKeys.getInstance().getDatabase();
    }

    public static String getDBString() {
        return String.format("/dbs/%s/", CosmosAccessKeys.getInstance().getDatabase());
    }

    public static String getCollectionString(String col) {
        return String.format("/dbs/%s/colls/%s", CosmosAccessKeys.getInstance().getDatabase(), col);
    }

    public static String getDocumentString(String col, String doc) {
        return String.format("/dbs/%s/colls/%s/docs/%s", CosmosAccessKeys.getInstance().getDatabase(), col, doc);
    }

    public static String getSQLQueryFor(String table, String field, String value) throws InvalidSQLQuery {
        if (table.matches(advanced_sql_injection_protection) && value.matches(advanced_sql_injection_protection))
            return String.format("SELECT * FROM %s u WHERE u.%s = '%s'", table, field, value);
        throw new InvalidSQLQuery();
    }

    public static String getSQLQueryForLikes(String table, String field, String value) throws InvalidSQLQuery {
        if (table.matches(advanced_sql_injection_protection) && value.matches(advanced_sql_injection_protection))
            return String.format("SELECT * FROM %s u WHERE u.%s = '%s'", table, field, value);
        throw new InvalidSQLQuery();
    }

    public static boolean verifyDocument(ResourceID doc) {
        return doc.getID().matches(advanced_sql_injection_protection);
    }

    public static boolean verifyValues(String... values) {
        for (String v : values) {
            if (!v.matches(advanced_sql_injection_protection)) {
                return false;
            }
        }
        return true;
    }
}
