package scc.utils;


import javax.activation.UnsupportedDataTypeException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class FileTypeParser {
    private static final Set<String> image_types = new HashSet<>(Arrays.asList("png", "jpg", "gif", "jpeg", "tiff", "bmp"));
    private static final Set<String> video_types = new HashSet<>(Arrays.asList("mp4", "mkv", "avi", "webm", "gifv", "mov", "m4v"));

    // return extension and new filename
    public static Pair<String, String> parseType(String filename, String new_filename) throws UnsupportedDataTypeException {

        int idx = filename.indexOf(".");
        String type = null;
        String file_id = null;
        if (idx != -1) {
            String extension = filename.substring(idx + 1).toLowerCase();
            file_id = new_filename + "." + extension;
            if (image_types.contains(extension)) {
                type = "image/" + extension;
            } else if (video_types.contains(extension)) {
                type = "video/" + extension;
            }

        }
        if (type == null)
            throw new UnsupportedDataTypeException();

        return new Pair<>(type, file_id);
    }

}
