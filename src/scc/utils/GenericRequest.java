package scc.utils;

import com.google.gson.Gson;
import scc.exceptions.CouldNotAddResource;
import scc.exceptions.InvalidParameters;
import scc.exceptions.InvalidSQLQuery;
import scc.exceptions.ResourceNotFound;
import scc.resources.ResourceID;

import javax.ws.rs.WebApplicationException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.logging.Logger;

public abstract class GenericRequest {
    private static final Gson g = new Gson();
    private static final Logger logger = Logger.getLogger(GenericRequest.class.getName());

    public static <T extends ResourceID> String add(String resource_name, T document) throws InvalidParameters, CouldNotAddResource {

        if (document.getID() == null)
            document.setID(IDGenerator.get());

        if (!DBClient.verifyDocument(document)) {
            logger.warning("ADD -> Resource has invalid parameters");
            throw new InvalidParameters();
        }


        // add to DB
        String result = DBRequest.addResource(resource_name, document);

        // add to Cache
        if (result != null) {
            CacheClient.put(resource_name + document.getID(), g.toJson(document));
        }

        return result;
    }

    public static <T extends ResourceID> void update(String resource_name, T document) throws InvalidParameters, ResourceNotFound {

        if (!DBClient.verifyDocument(document)) {
            logger.warning("UPDATE -> Resource has invalid parameters");
            throw new InvalidParameters();
        }

        // update DB
        DBRequest.updateResource(resource_name, document);

        // update Cache
        CacheClient.del(resource_name + document.getID());
        CacheClient.put(resource_name + document.getID(), g.toJson(document));
    }

    public static void delete(String resource_name, String id) throws WebApplicationException, ResourceNotFound, InvalidParameters {

        if (!DBClient.verifyValues(id)) {
            logger.warning("DELETE -> Resource has invalid parameters");
            throw new InvalidParameters();
        }

        // delete from DB
        DBRequest.deleteResource(resource_name, id);

        // delete from cache
        CacheClient.del(resource_name + id);
    }

    public static <T extends ResourceID> T get(String resource_name, String id, Type type) throws WebApplicationException, ResourceNotFound, InvalidParameters {

        return get(resource_name, id, type, true);

    }

    public static <T extends ResourceID> T get(String resource_name, String id, Type type, boolean useDB) throws WebApplicationException, ResourceNotFound, InvalidParameters {

        if (!DBClient.verifyValues(id)) {
            logger.warning("GET -> Resource has invalid parameters");
            throw new InvalidParameters();
        }

        // try to get from cache
        String result = CacheClient.get(resource_name + id);

        if (result != null) {
            return g.fromJson(result, type);
        }

        // not found get from DB
        if (useDB)
            return DBRequest.getResource(resource_name, id, type);
        else
            return null;

    }

    public static <T extends ResourceID> List<T> gets(String resource_name, String field, String id, Type type) throws InvalidSQLQuery, InvalidParameters {

        if (!DBClient.verifyValues(id)) {
            logger.warning("GETS -> Resource has invalid parameters");
            throw new InvalidParameters();
        }

        return DBRequest.getResources(resource_name, field, id, type);

    }
}
