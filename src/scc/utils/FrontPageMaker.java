package scc.utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.microsoft.azure.cosmosdb.Document;
import com.microsoft.azure.cosmosdb.FeedOptions;
import com.microsoft.azure.cosmosdb.FeedResponse;
import com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient;
import scc.resources.Post;
import scc.resources.PostLikes;

import java.util.*;

public class FrontPageMaker {

    public static final String FRONT_PAGE_PREFIX_KEY = "front_page_";
    private static final long POST_FRESHNESS_TIME_PERIOD = 1000 * 60 * 60 * 8; // 8 hours in milliseconds
    private static final Gson g = new Gson();

    public static String make(String location) {

        String prefix = FRONT_PAGE_PREFIX_KEY + (location == null ? "" : location);

        long oldestPostTime = System.currentTimeMillis() - POST_FRESHNESS_TIME_PERIOD;
        String query = String.format(
                "SELECT p.id, p.likeCount FROM PostLikes p WHERE p.postDate > %d %s ORDER BY p.likeCount DESC",
                oldestPostTime, location == null ? "" : ("AND p.location = '" + location + "'"));

        AsyncDocumentClient client = DBClient.get();
        FeedOptions queryOptions = new FeedOptions();
        queryOptions.setEnableCrossPartitionQuery(true);
        queryOptions.setMaxDegreeOfParallelism(-1);
        queryOptions.setMaxItemCount(250);

        String collection = DBClient.getCollectionString(PostLikes.RESOURCE_NAME);
        List<PostLikes> values = new LinkedList<>();

        Iterator<FeedResponse<Document>> it = client.queryDocuments(collection, query, queryOptions)
                .toBlocking()
                .getIterator();

        while (it.hasNext())
            for (Document d : it.next().getResults()) {
                values.add(g.fromJson(d.toJson(), PostLikes.class));
            }

        JsonArray arr = new JsonArray();

        // If postlikes is empty, query post by age only
        // TODO if post likes has a size smaller than 250 -> append from recent Posts
        // TODO avoid repetition. For cases where post likes return a very small amount of Posts
        if (values.isEmpty()) {
            query = String.format("SELECT * FROM Posts p %s ORDER BY p.creationTime DESC",
                    location == null ? "" : ("WHERE p.location = '" + location + "'"));

            collection = DBClient.getCollectionString(Post.RESOURCE_NAME);

            it = client.queryDocuments(collection, query, queryOptions)
                    .toBlocking()
                    .getIterator();

            while (it.hasNext())
                for (Document d : it.next().getResults()) {
                    Post elm = g.fromJson(d.toJson(), Post.class);
                    JsonObject json = g.toJsonTree(elm).getAsJsonObject();
                    arr.add(json);
                }
        } else {
            StringJoiner join = new StringJoiner(", ");
            for (PostLikes like : values) {
                join.add("'" + like.getID() + "'");
            }

            collection = DBClient.getCollectionString(Post.RESOURCE_NAME);
            query = String.format("SELECT * FROM Posts p WHERE p.id IN (%s)", join.toString());

            it = client.queryDocuments(collection, query, queryOptions).toBlocking().getIterator();

            while (it.hasNext())
                for (Document d : it.next().getResults()) {
                    Post elm = g.fromJson(d.toJson(), Post.class);
                    JsonObject json = g.toJsonTree(elm).getAsJsonObject();

                    Optional<PostLikes> optional = values.stream()
                            .filter(postLikes -> postLikes.getID().equals(d.getId()))
                            .findFirst();
                    int likes = optional.isPresent() ? optional.get().getLikeCount() : 0;
                    json.addProperty(PostLikes.LIKE_COUNT_FIELD_NAME, likes);
                    arr.add(json);
                }
        }

        int postCount = 0;
        int frontPage = 0;
        JsonArray currPage = new JsonArray();

        for (JsonElement post : arr) {
            currPage.add(post);
            postCount++;
            if (postCount == 25) {
                CacheClient.put(prefix + frontPage++, g.toJson(currPage));
                return g.toJson(currPage);
                //currPage = new JsonArray();
                //postCount = 0;
            }
        }
        if (postCount != 0) {
            CacheClient.put(prefix + frontPage, g.toJson(currPage));
        }

        return g.toJson(currPage);
    }
}
