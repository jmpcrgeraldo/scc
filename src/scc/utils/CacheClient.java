package scc.utils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import scc.config.RedisAccessKeys;

import java.time.Duration;
import java.util.Collections;
import java.util.List;

public class CacheClient {
    private static boolean DEACTIVATE = true;
    private static long EXPIRE_TIME = 1000 * 60 * 30; // 30 minutes
    private static CacheClient client = null;
    private final JedisPool pool;

    private CacheClient() {
        final JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(128);
        poolConfig.setMaxIdle(128);
        poolConfig.setMinIdle(16);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
        poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
        poolConfig.setNumTestsPerEvictionRun(3);
        poolConfig.setBlockWhenExhausted(true);
        //pool = new JedisPool(poolConfig, RedisAccessKeys.getInstance().getHostname(), 6380, 1000, RedisAccessKeys.getInstance().getCacheKey(), true);
        pool = new JedisPool(poolConfig, RedisAccessKeys.getInstance().getHostname(), 6379, 1000);
    }

    private static synchronized CacheClient getCache() {
        if (client == null) {
            client = new CacheClient();
        }
        return client;
    }

    public static void put(String key, String value) {
        if (DEACTIVATE) return;
        try (Jedis jedis = CacheClient.getCache().getJedisPool().getResource()) {
            jedis.set(key, value);
            jedis.expireAt(key, System.currentTimeMillis() + EXPIRE_TIME);
        } catch (Exception e) {
            return;
        }
    }

    public static void put(String key, String value, long ttl) {
        if (DEACTIVATE) return;
        try (Jedis jedis = CacheClient.getCache().getJedisPool().getResource()) {
            jedis.set(key, value);
            jedis.expireAt(key, System.currentTimeMillis() + ttl);
        } catch (Exception e) {
            return;
        }
    }

    public static void del(String key) {
        if (DEACTIVATE) return;
        try (Jedis jedis = CacheClient.getCache().getJedisPool().getResource()) {
            jedis.del(key);
        } catch (Exception e) {
            return;
        }
    }

    public static String get(String key) {
        if (DEACTIVATE) return null;
        try (Jedis jedis = CacheClient.getCache().getJedisPool().getResource()) {
            return jedis.get(key);
        } catch (Exception e) {
            return null;
        }
    }

    public static void clear() {
        try (Jedis jedis = CacheClient.getCache().getJedisPool().getResource()) {
            jedis.flushAll();
        } catch (Exception e) {
            return;
        }
    }

    public static List<String> getList(String key) {
        if (DEACTIVATE) return Collections.emptyList();
        try (Jedis jedis = CacheClient.getCache().getJedisPool().getResource()) {
            return jedis.lrange(key, 0, -1);
        } catch (Exception e) {
            return null;
        }
    }

    public static void addToList(String key, String value) {
        if (DEACTIVATE) return;
        try (Jedis jedis = CacheClient.getCache().getJedisPool().getResource()) {
            if (getList(key).isEmpty())
                jedis.expireAt(key, System.currentTimeMillis() + EXPIRE_TIME);
            jedis.lpush(key, value);
        } catch (Exception e) {
            return;
        }
    }

    private JedisPool getJedisPool() {
        return pool;
    }
}
