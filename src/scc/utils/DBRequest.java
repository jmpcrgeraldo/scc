package scc.utils;

import com.google.gson.Gson;
import com.microsoft.azure.cosmosdb.*;
import com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient;
import rx.Observable;
import scc.exceptions.CouldNotAddResource;
import scc.exceptions.InvalidSQLQuery;
import scc.exceptions.ResourceNotFound;
import scc.resources.ResourceID;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public abstract class DBRequest {
    private static final Gson g = new Gson();
    private static final Logger logger = Logger.getLogger(DBRequest.class.getName());

    static <T extends ResourceID> String addResource(String collection_name, T document) throws CouldNotAddResource {
        AsyncDocumentClient client = DBClient.get();
        String collection = DBClient.getCollectionString(collection_name);

        try {
            Observable<ResourceResponse<Document>> resp = client.createDocument(collection, document, null, false);
            return resp.toBlocking().first().getResource().getId();
        } catch (Exception e) {
            throw new CouldNotAddResource();
        }
    }

    public static void clearResources() {
        AsyncDocumentClient client = DBClient.get();
        List<DocumentCollection> collectionList = client.queryCollections(DBClient.getDBString(),
                "SELECT * FROM root r ", null).toBlocking().first().getResults();

        for (DocumentCollection c : collectionList) {
            //c.setDefaultTimeToLive(1);
            client.deleteCollection(DBClient.getCollectionString((String) c.get("id")), null).toCompletable().await();
            client.createCollection(DBClient.getDBString(), c, null).toCompletable().await();
            logger.info("Deleting container " + c.getId());
        }
    }

    static <T extends ResourceID> void updateResource(String collection_name, T document) throws ResourceNotFound {
        AsyncDocumentClient client = DBClient.get();
        String documentLink = DBClient.getDocumentString(collection_name, document.getID());
        RequestOptions options = new RequestOptions();

        options.setPartitionKey(new PartitionKey(document.getID()));

        try {
            Observable<ResourceResponse<Document>> resp = client.replaceDocument(documentLink, document, options);
            resp.toBlocking().first();
        } catch (Exception e) {
            throw new ResourceNotFound();
        }
    }

    static void deleteResource(String collection_name, String id) throws ResourceNotFound {
        AsyncDocumentClient client = DBClient.get();
        String documentLink = DBClient.getDocumentString(collection_name, id);
        RequestOptions options = new RequestOptions();
        options.setPartitionKey(new PartitionKey(id));

        try {
            Observable<ResourceResponse<Document>> resp = client.deleteDocument(documentLink, options);
            resp.toBlocking().first();
        } catch (Exception e) {
            throw new ResourceNotFound();
        }
    }

    static <T> T getResource(String collection_name, String id, Type class_type) throws ResourceNotFound {
        AsyncDocumentClient client = DBClient.get();
        FeedOptions queryOptions = new FeedOptions();
        queryOptions.setEnableCrossPartitionQuery(true);
        queryOptions.setMaxDegreeOfParallelism(-1);
        String document = DBClient.getDocumentString(collection_name, id);

        RequestOptions options = new RequestOptions();
        options.setPartitionKey(new PartitionKey(id));

        Observable<ResourceResponse<Document>> response = client.readDocument(document, options);

        try {
            Document d = response.toBlocking().first().getResource();
            return g.fromJson(d.toJson(), class_type);
        } catch (Exception e) {
            throw new ResourceNotFound();
        }
    }

    static <T> List<T> getResources(String collection_name, String field, String id, Type class_type) throws InvalidSQLQuery {
        AsyncDocumentClient client = DBClient.get();
        FeedOptions queryOptions = new FeedOptions();
        queryOptions.setEnableCrossPartitionQuery(true);
        queryOptions.setMaxDegreeOfParallelism(-1);
        String collection = DBClient.getCollectionString(collection_name);
        List<T> values = new LinkedList<>();

        Iterator<FeedResponse<Document>> it = client.queryDocuments(collection,
                DBClient.getSQLQueryFor(collection_name, field, id), queryOptions)
                .toBlocking()
                .getIterator();
        while (it.hasNext())
            for (Document d : it.next().getResults()) {
                values.add(g.fromJson(d.toJson(), class_type));
            }
        logger.info("FOUND -> " + values.size() + " entities");
        return values;
    }
}
