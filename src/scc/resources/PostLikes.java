package scc.resources;

import java.util.List;

public class PostLikes extends ResourceID {
    public static final String RESOURCE_NAME = "PostLikes";
    public static final String LIKE_COUNT_FIELD_NAME = "likeCount";

    // id == post id
    private List<String> usernames;
    private Integer likeCount;
    private Long postDate;
    private String postLocation;

    public PostLikes() {
    }

    public List<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(List<String> users_id) {
        this.usernames = users_id;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer like_count) {
        this.likeCount = like_count;
    }

    public Long getPostDate() {
        return postDate;
    }

    public void setPostDate(Long postDate) {
        this.postDate = postDate;
    }

    public String getPostLocation() {
        return postLocation;
    }

    public void setPostLocation(String postLocation) {
        this.postLocation = postLocation;
    }
}
