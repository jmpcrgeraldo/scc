package scc.resources;

public abstract class ResourceID {
    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    private String id;

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }
}
