package scc.resources;


import java.util.List;

public class Post extends ResourceID {
    public static final String RESOURCE_NAME = "Posts";
    public static final String PARENT_ID_FIELD_NAME = "parentId";

    private String title;// = DBClient.NULL_STRING;
    private String community;
    private String creator;
    private Long creationTime;
    private String msg;
    private String imageId;// = DBClient.NULL_STRING;
    private String parentId;// = DBClient.NULL_STRING;
    private String location;// = DBClient.NULL_STRING;
    private List<String> tags;
    private Boolean nsfw;
    private List<Post> childPosts;

    public Post() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community_id) {
        this.community = community_id;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String user_id) {
        this.creator = user_id;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creation_time) {
        this.creationTime = creation_time;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String multimedia_id) {
        this.imageId = multimedia_id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parent_post_id) {
        this.parentId = parent_post_id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void addTag(String tag) {
        this.tags.add(tag);
    }

    public Boolean getNsfw() {
        return nsfw;
    }

    public void setNsfw(Boolean nsfw) {
        this.nsfw = nsfw;
    }

    public List<Post> getChildPosts() {
        return childPosts;
    }

    public void setChildPosts(List<Post> childPosts) {
        this.childPosts = childPosts;
    }
}
