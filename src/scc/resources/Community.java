package scc.resources;

public class Community extends ResourceID {
    public static final String RESOURCE_NAME = "Communities";

    // TODO add followers

    // id == name
    private String name;
    private String desc;
    private String creator;

    public Community() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
